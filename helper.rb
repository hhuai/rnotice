require 'erb'

def replace_file(file)
  system %Q{rm -rf "$HOME/.#{file.sub(/\.erb$/, '')}"}
  link_file(file)
end

def rm_file(file)
  system %Q{rm -fr #{file}}
end

def is_erb_file(file)
  file.end_with?('.erb')
end

def process_erb(file,bind, new_file_path=nil)
	new_file = nil
	if is_erb_file(file)
		new_file = file[0, file.length-4]
		new_file = File.join(new_file_path, File.basename(new_file)) if new_file_path
		template = ERB.new(File.read(file))
		File.open(new_file, "w+") { |f|
			f.write(template.result(bind))
		}
	end
	new_file
end

def process_file(src, des, options)
  continue = true
  options = {sudo: false}.merge(options)
  
  unless options[:abs_path]
    des = File.expand_path(des)
    src = File.expand_path("../#{src}",__FILE__)
  end

  cmd_prefix = ''
  if options[:sudo]
    cmd_prefix = 'sudo '
  end
    
  if is_erb_file(src)
      src = process_erb(src)
  end

  if File.exist?(des) || File.symlink?(des)
      print "overwrite #{des}? [yn] "
      case $stdin.gets.chomp
      when 'y'
        system %Q{#{cmd_prefix} rm #{des}}
      else
        puts 'skip...'
        continue = false
      end
  end

  if continue && block_given?
    yield src, des, cmd_prefix      
  end
end

def my_copy_file(src, des, options={})
  puts "copy file #{src} -> #{des}"
  process_file(src, des, options) do |src, des, cmd_prefix|
    system %Q{#{cmd_prefix} cp #{src} #{des}}   
  end
end

def link_file(src, des, options={})
  puts "link file #{src} -> #{des}"
  process_file(src, des, options) do |src, des, cmd_prefix|
    system %Q{#{cmd_prefix} ln -s #{src} #{des}}      
  end
end

def write_bash_profile(content)
  File.open(File.expand_path("~/.bash_profile"), "w+") do |f|
    f.write("\n"+content+"\n")
  end  
end

def is_mac?
  Config::CONFIG['host_os'].start_with? 'darwin'
end