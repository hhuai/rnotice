require 'eventmachine'
require 'faye'
require 'logger'
require 'terminal-notifier'

logger = Logger.new( File.expand_path('../log.txt',__FILE__) )

## http://ruby-china.org/api/users/temp_access_token?token=a89b0502cc16fb7a8648:917
url_path = "/notifications_count/0c36cc419a8b7faa69781f95f88ec791"
EM.run do
	client = Faye::Client.new('http://ruby-china.org:8080/faye')
	client.subscribe(url_path) do |message|
		logger.info message.inspect
		puts message.inspect
		TerminalNotifier.notify('hello world', :title => 'from ruby-china')
	end
	publication = client.publish(url_path,{})
#	publication.callback do |info|
#		puts "[PUBLISH SUCCEEDED] #{publication.inspect}"
#		##EM.stop_event_loop
#	end
#	publication.errback do |error|
#		puts "[PUBLISH FAILED] #{error.inspect}"
#		EM.stop_event_loop
#	end
end
